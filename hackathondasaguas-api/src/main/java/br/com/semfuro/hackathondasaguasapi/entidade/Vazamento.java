package br.com.semfuro.hackathondasaguasapi.entidade;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.gaspar.utils.entidade.EntidadeBase;

@Entity
@NamedQueries({
	@NamedQuery(name="Vazamento.buscarTodos", query="select obj from Vazamento obj order by obj.data"),
	@NamedQuery(name="Vazamento.buscarTodosPorData", query="select obj from Vazamento obj where obj.data = :p0 order by obj.data")
})
public class Vazamento extends EntidadeBase {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "Vazamento", sequenceName = "vazamento_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "Vazamento")
	private Long id;
	
	private String latitude;
	
	private String longitude;
	
	private String medicao;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date data;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getMedicao() {
		return medicao;
	}

	public void setMedicao(String medicao) {
		this.medicao = medicao;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	

}
