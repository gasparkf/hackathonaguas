package br.com.semfuro.hackathondasaguasapi.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.gaspar.framework.persistencia.jpa.BaseDAO;
import br.com.semfuro.hackathondasaguasapi.entidade.Vazamento;

@Stateless
public class VazamentoDAO extends BaseDAO<Vazamento>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@PersistenceContext
	@Override
	protected void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	

}
