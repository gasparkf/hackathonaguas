package br.com.semfuro.hackathondasaguasapi.entidade;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.gaspar.utils.entidade.EntidadeBase;

@Entity
@NamedQueries({
	@NamedQuery(name="Evento.buscarTodos", query="select obj from Evento obj order by obj.dataCriacao"),
	@NamedQuery(name="Evento.buscarTodosPorStatus", query="select obj from Evento obj where obj.status = :p0 order by obj.dataCriacao"),
	@NamedQuery(name="Evento.buscarTodosPorEndereco", query="select obj from Evento obj where obj.endereco = :p0 order by obj.dataCriacao")
})
public class Evento extends EntidadeBase {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@SequenceGenerator(name = "Evento", sequenceName = "evento_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "Evento")
	private Long id;
	
	private String endereco;
	
	private String numero;
	
	private String bairro;
	
	@Column(name="agua_esgoto")
	private String aguaEsgoto;
	
	@Column(name="rua_relogio")
	private String ruaRelogio;
	
	private String email;
	
	private String telefone;
	
	private String nome;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_criacao")
	private Date dataCriacao = new Date();
	
	private String status = "ABERTO";
	
	private String foto1;
	
	private String foto2;
	
	private String foto3;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getAguaEsgoto() {
		return aguaEsgoto;
	}

	public void setAguaEsgoto(String aguaEsgoto) {
		this.aguaEsgoto = aguaEsgoto;
	}

	public String getRuaRelogio() {
		return ruaRelogio;
	}

	public void setRuaRelogio(String ruaRelogio) {
		this.ruaRelogio = ruaRelogio;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Evento other = (Evento) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFoto1() {
		return foto1;
	}

	public void setFoto1(String foto1) {
		this.foto1 = foto1;
	}

	public String getFoto2() {
		return foto2;
	}

	public void setFoto2(String foto2) {
		this.foto2 = foto2;
	}

	public String getFoto3() {
		return foto3;
	}

	public void setFoto3(String foto3) {
		this.foto3 = foto3;
	}
	
	

}
