package br.com.semfuro.hackathondasaguasapi.bo;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.com.gaspar.framework.modelo.BaseBO;
import br.com.gaspar.framework.persistencia.IBaseDAO;
import br.com.semfuro.hackathondasaguasapi.dao.EventoDAO;
import br.com.semfuro.hackathondasaguasapi.entidade.Evento;

@Stateless
public class EventoBO extends BaseBO<Evento>{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private EventoDAO dao;
	
	@Override
	public IBaseDAO<Evento> getDAOPadrao() {
		return dao;
	}

}
