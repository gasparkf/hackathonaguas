package br.com.semfuro.hackathondasaguasapi.rest;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import br.com.gaspar.framework.modelo.IBaseBO;
import br.com.gaspar.framework.modelo.rs.BaseRS;
import br.com.semfuro.hackathondasaguasapi.bo.VazamentoBO;
import br.com.semfuro.hackathondasaguasapi.entidade.Vazamento;

public class VazamentoRS extends BaseRS<Vazamento>{
	
	@EJB
	private VazamentoBO bo;

	@Override
	public IBaseBO<Vazamento> getBO() {
		return bo;
	}

	@Override
	public String getNamedQuery() {
		return "Vazamento.buscarTodos";
	}
	
	@GET
	@Path("/ping")
	public String ping(){
		return "pong";
	}

}
