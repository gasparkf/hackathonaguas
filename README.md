# Grupo Sem Furo

Nome do projeto: **hackathonaguas-api**

Desafio: **A**

Participantes:

- Maicon Dante Pasqualeto
- Emne Vanessa
- Percival Adriano
- Antonio Marcos
- Gaspar Kuhnen



# README #

Backend restfull para apoiar o aplicativo progressive web que serve como ferramenta para qualquer pessoa poder avisar sobre vazamentos de água e esgoto visívies.

### Licenca ###
Código fonte de licenca aberta.


### Versão ###

* versão 1.0-SNAPSHOT

### How do I get set up? ###

* Baixar e instalar Java JDK versão 8.x
	http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html
* Baixar e instalar Maven 3
	https://maven.apache.org/download.cgi
	executar mvn eclipse:eclipse ou utilizar plugin Maven para usa IDE favorita.
* Baixar e instalar no repositório local do maven os projetos framework
    https://bitbucket.org/gasparkf/framework/
* Baixar e instalar PostgreSQL
	https://www.postgresql.org/download/
* Criar um banco de dados

* Baixar e instalar Wildfly 10
	http://wildfly.org/downloads/
* configurar um dataSource JTA com JNDI name = java:/jdbc/hackathonDS apontando para o seu banco de dados.
* 
* 
* Deployment instructions
* executar mvn clean install package e publicar o arquivo .war no wildlfy

### Fale com o desenvolvedor
* Dúvidas ou sugestões? Envie um email para Gaspar - gasparkf@gmail.com