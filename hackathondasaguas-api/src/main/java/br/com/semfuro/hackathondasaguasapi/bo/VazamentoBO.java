package br.com.semfuro.hackathondasaguasapi.bo;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.com.gaspar.framework.modelo.BaseBO;
import br.com.gaspar.framework.persistencia.IBaseDAO;
import br.com.semfuro.hackathondasaguasapi.dao.VazamentoDAO;
import br.com.semfuro.hackathondasaguasapi.entidade.Vazamento;

@Stateless
public class VazamentoBO extends BaseBO<Vazamento>{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private VazamentoDAO dao;
	
	@Override
	public IBaseDAO<Vazamento> getDAOPadrao() {
		return dao;
	}

}
