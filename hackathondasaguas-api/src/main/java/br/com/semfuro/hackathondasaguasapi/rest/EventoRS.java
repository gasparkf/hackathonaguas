package br.com.semfuro.hackathondasaguasapi.rest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import br.com.gaspar.framework.modelo.IBaseBO;
import br.com.gaspar.framework.modelo.rs.BaseRS;
import br.com.gaspar.utils.exception.BaseException;
import br.com.semfuro.hackathondasaguasapi.bo.EventoBO;
import br.com.semfuro.hackathondasaguasapi.entidade.Evento;

@Path("/evento")
public class EventoRS extends BaseRS<Evento> {

	@EJB
	private EventoBO bo;

	@Override
	public IBaseBO<Evento> getBO() {
		return bo;
	}

	@Override
	public String getNamedQuery() {
		return "Evento.buscarTodos";
	}

	@GET
	@Path("/status/{status}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public List<Evento> buscarPorStatus(@PathParam(value = "status") String status) {
		List<Evento> lista = null;
		try {
			lista = getBO().buscarTodosPorNamedQuery("Evento.buscarTodosPorStatus", status);
		} catch (BaseException e) {
			e.printStackTrace();
		}

		return lista;
	}
	
	@GET
	@Path("/endereco/{endereco}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public List<Evento> buscarPorPEndereco(@PathParam(value = "endereco") String endereco) {
		List<Evento> lista = null;
		try {
			lista = getBO().buscarTodosPorNamedQuery("Evento.buscarTodosPorEndereco", endereco);
		} catch (BaseException e) {
			e.printStackTrace();
		}

		return lista;
	}

	@GET
	@Path("/status")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public List<Evento> buscarPorStatus() {
		List<Evento> lista = null;
		try {
			lista = buscarTodosPorNamedQuery();
		} catch (BaseException e) {
			e.printStackTrace();
		}

		return lista;
	}

	@POST
	@Path("/gravar")
	@Consumes("multipart/form-data")
	public Response gravar(MultipartFormDataInput input) {

		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		String nome = getProp(uploadForm, "nome");
		String endereco = getProp(uploadForm, "endereco");
		String numero = getProp(uploadForm, "numero");
		String bairro = getProp(uploadForm, "bairro");
		String telefone = getProp(uploadForm, "telefone");
		String ruaRelogio = getProp(uploadForm, "ruaRelogio");
		String aguaEsgoto = getProp(uploadForm, "aguaEsgoto");
		String email = getProp(uploadForm, "email");
		String nomeArquivo = null;
		// Get file data to save
		List<InputPart> inputParts = uploadForm.get("foto1");

		for (InputPart inputPart : inputParts) {
			try {

				MultivaluedMap<String, String> header = inputPart.getHeaders();
				nomeArquivo = getFileName(header);

				// convert the uploaded file to inputstream
				InputStream inputStream = inputPart.getBody(InputStream.class, null);

				byte[] bytes = org.apache.commons.io.IOUtils.toByteArray(inputStream);
				// constructs upload file path
				nomeArquivo = "/home/wildfly/upload/" + nomeArquivo;
				writeFile(bytes, nomeArquivo);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		Evento evento = new Evento();
		evento.setNome(nome);
		evento.setTelefone(telefone);
		evento.setEndereco(endereco);
		evento.setNumero(numero);
		evento.setBairro(bairro);
		evento.setEmail(email);
		evento.setRuaRelogio(ruaRelogio);
		evento.setAguaEsgoto(aguaEsgoto);
		evento.setFoto1(nomeArquivo);

		try {
			getBO().gravar(evento);
		} catch (BaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return Response.status(200).entity("Criado com sucesso!").build();

	}

	private String getFileName(MultivaluedMap<String, String> header) {

		String[] contentDisposition = header.getFirst("Content-Disposition").split(";");

		for (String filename : contentDisposition) {
			if ((filename.trim().startsWith("filename"))) {

				String[] name = filename.split("=");

				String finalFileName = name[1].trim().replaceAll("\"", "");
				return finalFileName;
			}
		}
		return "unknown";
	}

	// Utility method
	private void writeFile(byte[] content, String filename) throws IOException {
		File file = new File(filename);
		if (!file.exists()) {
			System.out.println("not exist> " + file.getAbsolutePath());
			file.createNewFile();
		}
		FileOutputStream fop = new FileOutputStream(file);
		fop.write(content);
		fop.flush();
		fop.close();
	}

	private String getProp(Map<String, List<InputPart>> uploadForm, String prop) {
		String valorProp = null;
		if (uploadForm.get(prop) != null) {
			try {
				valorProp = uploadForm.get(prop).get(0).getBodyAsString();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return valorProp;
	}

	@GET
	 @Path("/foto/{foto}")
	 @Produces("image/jpeg")
	    public Response getFileInJPEGFormat(@PathParam("foto") Long id){
		
			Evento evento = null;
			try {
				evento = getBO().buscarPorId(Evento.class, id);
			} catch (BaseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(evento == null){
				 ResponseBuilder response = Response.status(Status.BAD_REQUEST);
		            return response.build();
			}
			String foto = evento.getFoto1();
	        System.out.println("File requested is : " + foto);
	         
	        //Put some validations here such as invalid file name or missing file name
	        if(foto == null || foto.isEmpty()){
	            ResponseBuilder response = Response.status(Status.BAD_REQUEST);
	            return response.build();
	        }
	         
	        //Prepare a file object with file to return
	        File file = new File(foto);
	         
	        ResponseBuilder response = Response.ok((Object) file);
	        //response.header("Content-Disposition", "attachment; filename=foto.jpeg");
	        return response.build();
	    }
	
	@GET
	@Path("/id/{id}")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	public Evento buscarPorId(@PathParam("id") String id) throws BaseException {
		Evento entidade = getBO().buscarPorId(Evento.class, Long.parseLong(id));
		return entidade;
	}

}